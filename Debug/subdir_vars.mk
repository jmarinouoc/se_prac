################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../msp432p401r.cmd 

C_SRCS += \
../Crystalfontz128x128_ST7735.c \
../HAL_MSP_EXP432P401R_Crystalfontz128x128_ST7735.c \
../adc14_multiple_channel_no_repeat.c \
../i2c_driver.c \
../main.c \
../msp432_startup_ccs.c \
../system_msp432p401r.c 

C_DEPS += \
./Crystalfontz128x128_ST7735.d \
./HAL_MSP_EXP432P401R_Crystalfontz128x128_ST7735.d \
./adc14_multiple_channel_no_repeat.d \
./i2c_driver.d \
./main.d \
./msp432_startup_ccs.d \
./system_msp432p401r.d 

OBJS += \
./Crystalfontz128x128_ST7735.obj \
./HAL_MSP_EXP432P401R_Crystalfontz128x128_ST7735.obj \
./adc14_multiple_channel_no_repeat.obj \
./i2c_driver.obj \
./main.obj \
./msp432_startup_ccs.obj \
./system_msp432p401r.obj 

OBJS__QUOTED += \
"Crystalfontz128x128_ST7735.obj" \
"HAL_MSP_EXP432P401R_Crystalfontz128x128_ST7735.obj" \
"adc14_multiple_channel_no_repeat.obj" \
"i2c_driver.obj" \
"main.obj" \
"msp432_startup_ccs.obj" \
"system_msp432p401r.obj" 

C_DEPS__QUOTED += \
"Crystalfontz128x128_ST7735.d" \
"HAL_MSP_EXP432P401R_Crystalfontz128x128_ST7735.d" \
"adc14_multiple_channel_no_repeat.d" \
"i2c_driver.d" \
"main.d" \
"msp432_startup_ccs.d" \
"system_msp432p401r.d" 

C_SRCS__QUOTED += \
"../Crystalfontz128x128_ST7735.c" \
"../HAL_MSP_EXP432P401R_Crystalfontz128x128_ST7735.c" \
"../adc14_multiple_channel_no_repeat.c" \
"../i2c_driver.c" \
"../main.c" \
"../msp432_startup_ccs.c" \
"../system_msp432p401r.c" 


