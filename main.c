//*****************************************************************************
//
// Copyright (C) 2015 - 2016 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//  Redistributions of source code must retain the above copyright
//  notice, this list of conditions and the following disclaimer.
//
//  Redistributions in binary form must reproduce the above copyright
//  notice, this list of conditions and the following disclaimer in the
//  documentation and/or other materials provided with the
//  distribution.
//
//  Neither the name of Texas Instruments Incorporated nor the names of
//  its contributors may be used to endorse or promote products derived
//  from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//*****************************************************************************

// Includes standard
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

// Includes FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

// Includes drivers
#include "i2c_driver.h"
#include "temp_sensor.h"
#include "opt3001.h"
#include "tmp006.h"

#include "msp.h"
#include <driverlib.h>
#include <grlib.h>
#include "Crystalfontz128x128_ST7735.h"

#include "accelerometer_driver.h"
#include "adc14_multiple_channel_no_repeat.h"

// Definicion de parametros RTOS
#define WRITER_TASK_PRIORITY    1
#define READER_TASK_PRIORITY  1
#define HEARTBEAT_TASK_PRIORITY 1
#define QUEUE_SIZE  10
#define ONE_SECOND_MS  1000
#define HEART_BEAT_ON_MS 10
#define HEART_BEAT_OFF_MS 990
#define BUFFER_SIZE 10

#define READ_PERIOD_TEMP_MS     670
#define READ_PERIOD_LIGHT_MS    350
#define READ_PERIOD_ACCEL_MS    100


#define DEBUG_MSG 0

// UART Configuration Parameter (9600 bps - clock 8MHz)
const eUSCI_UART_Config uartConfig =
{
        EUSCI_A_UART_CLOCKSOURCE_SMCLK,          // SMCLK Clock Source
        78,                                      // BRDIV = 78
        1,                                       // UCxBRF = 2
        0,                                       // UCxBRS = 0
        EUSCI_A_UART_NO_PARITY,                  // No Parity
        EUSCI_A_UART_LSB_FIRST,                  // LSB First
        EUSCI_A_UART_ONE_STOP_BIT,               // One stop bit
        EUSCI_A_UART_MODE,                       // UART mode
        EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION  // Oversampling
};


// Prototipos de funciones privadas
static void prvSetupHardware(void);
static void prvTempWriterTask(void *pvParameters);
static void prvLightWriterTask(void *pvParameters);
static void prvAccelWriterTask(void *pvParameters);
static void prvReaderTask(void *pvParameters);
static void prvHeartBeatTask(void *pvParameters);


static void screenInit();
void drawTitle(void);
static void dadesInit();

static TickType_t writeAccel_delay;
// Declaracion de un mutex para acceso unico a I2C, UART y buffer
SemaphoreHandle_t xMutexBuff;

// Declaracion de un semaforo binario
volatile SemaphoreHandle_t xBinarySemaphore;
// Declaracion de una cola de mensajes
QueueHandle_t xQueue;

typedef enum Sensor
{
    light = 1,
    temp = 2,
    nada = 3
} Sensor;

// Queue element
typedef struct {
    Sensor sensor;
    float value;
} Queue_reg_t;

// Tipo de mensaje a almacenar en la cola
typedef struct {
    float acc_x;
    float acc_y;
    float acc_z;
} acc_t;

// Tipo de mitges
typedef struct {
    float tempMitjaDarrerMinut;
    float tempMitjaminutAnterior;
} mitges_t;

/* Graphic library context */
Graphics_Context g_sContext;

// Buffer de mensajes
Queue_reg_t buffer[BUFFER_SIZE];
uint8_t buff_pos;


int cont_temp_segons = 0;


int cont_mitjaAccel = 0;
int cont_mitjaLight = 0;
//Creem mitjes
 mitges_t registermitges;

int main(void){

    // Inicializacion del semaforo binario
    xBinarySemaphore = xSemaphoreCreateBinary();
    // Inicializacion de la cola
    xQueue = xQueueCreate( QUEUE_SIZE, sizeof( acc_t ) );
    // Inicializacio de mutex
    xMutexBuff= xSemaphoreCreateMutex();

    writeAccel_delay = pdMS_TO_TICKS( READ_PERIOD_ACCEL_MS);
    MAP_Interrupt_enableMaster();

    //Inicialitzem dades
    dadesInit();

    // Comprueba si semaforo y mutex se han creado bien
    if ((xBinarySemaphore != NULL) && (xMutexBuff != NULL))
    {
        // Inicializacion del hardware
        prvSetupHardware();

        // Creacion de tareas
        xTaskCreate( prvTempWriterTask, "TempWriterTask", configMINIMAL_STACK_SIZE, NULL,
                     WRITER_TASK_PRIORITY,NULL );
        xTaskCreate( prvLightWriterTask, "LightWriterTask", configMINIMAL_STACK_SIZE, NULL,
                    WRITER_TASK_PRIORITY, NULL );

        xTaskCreate( prvAccelWriterTask, "AccelWriterTask", configMINIMAL_STACK_SIZE, NULL,
                            WRITER_TASK_PRIORITY, NULL );
        xTaskCreate( prvReaderTask, "ReaderTask", configMINIMAL_STACK_SIZE, NULL,
                    READER_TASK_PRIORITY, NULL );
        xTaskCreate( prvHeartBeatTask, "HeartBeatTask", configMINIMAL_STACK_SIZE, NULL,
                     HEARTBEAT_TASK_PRIORITY, NULL );

        vTaskStartScheduler();
    }
    // Solo llega aqui si no hay suficiente memoria
    // para iniciar el scheduler
    return 0;
}

// Inicializacion del hardware del sistema
static void prvSetupHardware(void){

    extern void FPU_enableModule(void);

    // Configuracion del pin P1.0 - LED 1 - como salida y puesta en OFF
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN0);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P1, GPIO_PIN0);

    // Inicializacion de pins sobrantes para reducir consumo
    MAP_GPIO_setAsOutputPin(GPIO_PORT_P2, PIN_ALL8);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PB, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PC, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PD, PIN_ALL16);
    MAP_GPIO_setAsOutputPin(GPIO_PORT_PE, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_P2, PIN_ALL8);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PB, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PC, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PD, PIN_ALL16);
    MAP_GPIO_setOutputLowOnPin(GPIO_PORT_PE, PIN_ALL16);

    // Habilita la FPU
    MAP_FPU_enableModule();
    // Cambia el numero de "wait states" del controlador de Flash
    MAP_FlashCtl_setWaitState(FLASH_BANK0, 2);
    MAP_FlashCtl_setWaitState(FLASH_BANK1, 2);

    // Selecciona la frecuencia central de un rango de frecuencias del DCO
    MAP_CS_setDCOCenteredFrequency(/*CS_DCO_FREQUENCY_6*/CS_DCO_FREQUENCY_12);
    // Configura la frecuencia del DCO
    CS_setDCOFrequency(CS_8MHZ);

    // Inicializa los clocks HSMCLK, SMCLK, MCLK y ACLK
    MAP_CS_initClockSignal(CS_HSMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
    MAP_CS_initClockSignal(CS_ACLK, CS_REFOCLK_SELECT, CS_CLOCK_DIVIDER_1);

    // Selecciona el nivel de tension del core
    MAP_PCM_setCoreVoltageLevel(PCM_VCORE0);

    // Inicializacion del I2C
    I2C_init();
    // Inicializacion del sensor TMP006
    TMP006_init();
    // Inicializacion del sensor opt3001
    sensorOpt3001Init();
    sensorOpt3001Enable(true);

    //Configurem LCD
    screenInit();
    // Inicializa acelerometro+ADC
    init_Accel();
    MAP_Interrupt_enableMaster();
}

//Tarea heart beat
static void prvHeartBeatTask (void *pvParameters){
    for(;;){
        MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);
        vTaskDelay( pdMS_TO_TICKS(HEART_BEAT_ON_MS) );
        MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN0);
        vTaskDelay( pdMS_TO_TICKS(HEART_BEAT_OFF_MS) );
    }
}

//Tarea lectura temperatura
static void prvTempWriterTask (void *pvParameters){
    // Resultado del envio a la cola
    Queue_reg_t queueRegister;
    float temperature;

    for(;;){
        vTaskDelay( pdMS_TO_TICKS(READ_PERIOD_TEMP_MS ) );

        cont_temp_segons++;
        // Lee el valor de la medida de temperatura
        temperature = TMP006_readAmbientTemperature();

        queueRegister.sensor = temp;
        queueRegister.value = temperature;

        if(xSemaphoreTake(xMutexBuff,portMAX_DELAY)){
            buff_pos++;
            buffer[buff_pos % BUFFER_SIZE] = queueRegister;
            xSemaphoreGive(xMutexBuff);
        }
    }
}

//Tarea lectura luz
static void prvLightWriterTask (void *pvParameters){

    // Resultado del envio a la cola
    Queue_reg_t queueRegister;

    uint16_t rawData;
    float convertedLux;

    for(;;){
        vTaskDelay(pdMS_TO_TICKS(READ_PERIOD_LIGHT_MS));

        // Lee el valor de la medida de luz
        sensorOpt3001Read(&rawData);
        sensorOpt3001Convert(rawData, &convertedLux);

        queueRegister.sensor = light;
        queueRegister.value = convertedLux;

        if(xSemaphoreTake(xMutexBuff,portMAX_DELAY)){
            buff_pos++;
            buffer[buff_pos % BUFFER_SIZE] = queueRegister;
            xSemaphoreGive(xMutexBuff);
        }
    }
}

// Tarea Accelerometer
static void prvAccelWriterTask  (void *pvParameters)
{
    // Resultado del envio a la cola
    acc_t queue_element;
    float Datos[NUM_ADC_CHANNELS];
    xSemaphoreTake( xBinarySemaphore, 0 );
    // La tarea se repite en un bucle infinito
    for(;;) {
        Accel_read(Datos);
        queue_element.acc_x = Datos[0];
        queue_element.acc_y = Datos[1];
        queue_element.acc_z = Datos[2];
        xQueueSend(xQueue, &queue_element, portMAX_DELAY);
        vTaskDelay(writeAccel_delay);
    }
}
//Tarea lectura de cola
static void prvReaderTask (void *pvParameters)
{
    Queue_reg_t queueRegister;
    acc_t queue_element;
    acc_t queue_element_mitja;

    float valueCalcMitjaTemp = 0;
    float valueCalcMitjaLight = 0;
    float diff_temp = 0;

    char message[50];
    char value[10];
    //static int cont = 0;
    for(;;){

        vTaskDelay( pdMS_TO_TICKS(/*ONE_SECOND_MS*/650));
            if(xSemaphoreTake(xMutexBuff,portMAX_DELAY)){
                for(int i=0; i<BUFFER_SIZE; i++){
                    queueRegister = buffer[i];
                    if (queueRegister.sensor==light){
                        strcpy(message, "\nluz: ");
                        if ( cont_mitjaLight == 0){valueCalcMitjaLight = queueRegister.value;}
                        valueCalcMitjaLight = (valueCalcMitjaLight + queueRegister.value) * 0.5;
                        cont_mitjaLight++;
                    }else if(queueRegister.sensor==temp){
                        valueCalcMitjaTemp = (valueCalcMitjaTemp) * 0.5  + (queueRegister.value) * 0.5;
                    }
                    if (queueRegister.sensor==light &&  cont_mitjaLight > 1){
                       ftoa(valueCalcMitjaLight, value, 2);
                       strcat(strcat(message,value)," ");
                       Graphics_drawStringCentered(&g_sContext, (int8_t *)message, AUTO_STRING_LENGTH, 64, 30, OPAQUE_TEXT);
                       //reset comptador de dos lectures per fer la mitja
                       cont_mitjaLight = 0;
                    }else if(queueRegister.sensor==temp)
                    {
                        //calculem primer minut
                        if (cont_temp_segons  == 59)
                        {
                                strcpy(message,"\ntemp: ");
                                registermitges.tempMitjaDarrerMinut = valueCalcMitjaTemp;
                                ftoa(registermitges.tempMitjaDarrerMinut, value, 2);
                                strcat(strcat(message,value)," ");
                                Graphics_drawStringCentered(&g_sContext, (int8_t *)message, AUTO_STRING_LENGTH, 64, 40, OPAQUE_TEXT);

                        }
                        //La resta de minuts
                        else if (cont_temp_segons > 119)
                        {
                                registermitges.tempMitjaminutAnterior = registermitges.tempMitjaDarrerMinut;
                                registermitges.tempMitjaDarrerMinut = valueCalcMitjaTemp;
                                strcpy(message,"\ntemp: ");
                                ftoa(registermitges.tempMitjaDarrerMinut, value, 2);
                                strcat(strcat(message,value)," ");
                                Graphics_drawStringCentered(&g_sContext, (int8_t *)message, AUTO_STRING_LENGTH, 64, 40, OPAQUE_TEXT);
                                diff_temp = registermitges.tempMitjaDarrerMinut - registermitges.tempMitjaminutAnterior;
                                strcpy(message,"\ndiff temp: ");
                                ftoa( diff_temp, value, 2);
                                strcat(strcat(message,value)," ");
                                Graphics_drawStringCentered(&g_sContext, (int8_t *)message, AUTO_STRING_LENGTH, 64, 50, OPAQUE_TEXT);
                                //reset comptador de minuts
                                cont_temp_segons = 60;
                        }

                    }

                    //Calcular Valor mig d�acceleraci�
                    if(xQueueReceive( xQueue, &queue_element, portMAX_DELAY )== pdPASS )
                    {
                        //controllem el primer valor de la mitja
                        if (cont_mitjaAccel == 0)
                        {
                            float a = (0.0001 * registermitges.tempMitjaDarrerMinut);
                            queue_element_mitja.acc_x = queue_element.acc_x + (0.0001 * registermitges.tempMitjaDarrerMinut);
                            queue_element_mitja.acc_y = queue_element.acc_y + (0.0001 * registermitges.tempMitjaDarrerMinut);
                            queue_element_mitja.acc_z = queue_element.acc_z + (0.0004 * registermitges.tempMitjaDarrerMinut);
                            cont_mitjaAccel ++;
                        }
                        else
                        {
                            cont_mitjaAccel++;
                            queue_element_mitja.acc_x = (queue_element_mitja.acc_x + (queue_element.acc_x + (0.0001 * registermitges.tempMitjaDarrerMinut)))/2;
                            queue_element_mitja.acc_y = (queue_element_mitja.acc_y + (queue_element.acc_y + (0.0001 * registermitges.tempMitjaDarrerMinut)))/2;
                            queue_element_mitja.acc_z = (queue_element_mitja.acc_z + (queue_element.acc_z + (0.0004 * registermitges.tempMitjaDarrerMinut)))/2;
                        }

                     }
                    if (cont_mitjaAccel > QUEUE_SIZE -1){
                            Graphics_drawStringCentered(&g_sContext, "Accel:", AUTO_STRING_LENGTH, 64, 70, OPAQUE_TEXT);
                            strcpy(message,"\nX: ");
                            ftoa(queue_element_mitja.acc_x, value, 2);
                            strcat(strcat(message,value)," ");
                            Graphics_drawStringCentered(&g_sContext, (int8_t *)message, AUTO_STRING_LENGTH, 64, 80, OPAQUE_TEXT);
                            strcpy(message,"\nY: ");
                            ftoa(queue_element_mitja.acc_y, value, 2);
                            strcat(strcat(message,value)," ");
                            Graphics_drawStringCentered(&g_sContext, (int8_t *)message, AUTO_STRING_LENGTH, 64, 90, OPAQUE_TEXT);
                            strcpy(message,"\nZ: ");
                            ftoa(queue_element_mitja.acc_z, value, 2);
                            strcat(strcat(message,value)," ");
                            Graphics_drawStringCentered(&g_sContext, (int8_t *)message, AUTO_STRING_LENGTH, 64, 100, OPAQUE_TEXT);
                    }
                }
                cont_mitjaAccel=0;
                xSemaphoreGive(xMutexBuff);
                taskYIELD();
            }

    }
}


static void screenInit()
{
    /* Halting WDT and disabling master interrupts */
     MAP_WDT_A_holdTimer();
     MAP_Interrupt_disableMaster();

    /* Initializes display */
    Crystalfontz128x128_Init();
    /* Set default screen orientation */
    Crystalfontz128x128_SetOrientation(LCD_ORIENTATION_UP);
    /* Initializes graphics context */
    Graphics_initContext(&g_sContext, &g_sCrystalfontz128x128);
    Graphics_setForegroundColor(&g_sContext, GRAPHICS_COLOR_RED);
    Graphics_setBackgroundColor(&g_sContext, GRAPHICS_COLOR_WHITE);
    GrContextFontSet(&g_sContext, &g_sFontFixed6x8);

    drawTitle();
}

/*
 * Clear display and redraw title + accelerometer data
 */
void drawTitle()
{
    Graphics_clearDisplay(&g_sContext);
    Graphics_drawStringCentered(&g_sContext,
                                    "PRAC_JCM_57",
                                    AUTO_STRING_LENGTH,
                                    64,
                                    10,
                                    OPAQUE_TEXT);

}

/*
 * Inicialitzar dades
*/
static void dadesInit()
{
    registermitges.tempMitjaDarrerMinut = 20;
    registermitges.tempMitjaminutAnterior = 0;

    // Inicializacion buffer
    for(int i=0; i<BUFFER_SIZE;i++){
        buffer[i].sensor = nada;
        buffer[i].value = -1.0;
    }
    buff_pos = 0;
}
